﻿using System;
using NUnit.Framework;
using PlanetarySystem.Planetary;

namespace PlanetarySystem.Test
{
    [TestFixture]
    class PlanetarySystemTest
    {
        [Test]
        public void TestSimple()
        {
            PlanetarySystem<ISimulationElement> ps = new PlanetarySystem<ISimulationElement>();
            Planet p1 = new Planet(1.0, 0.0, 0.1);
            ps.Append(p1);
            PointPlanetarySystem<Planet> psKomponent = new PointPlanetarySystem<Planet>();
            Planet p2 = new Planet(1.0, 1.0, 0.1);
            psKomponent.Append(p2);
            psKomponent.Append(new Planet(2.0, 1.0, 0.1));
            Assert.That(psKomponent.Distance(0, 1), Is.EqualTo(1.0).Within(0.0001));
            ps.Append(psKomponent);
            ps.Tick();
            Assert.That(p1.GetTheta(), Is.EqualTo(0.1).Within(0.0001));
         
            Assert.That(psKomponent.GetElement(0).GetTheta(), Is.EqualTo((Math.PI / 4) + 0.1).Within(0.0001));
            Assert.That(psKomponent.GetElement(0).GetOmega(), Is.EqualTo(0.1).Within(0.0001));
            psKomponent.Append(new Planet(2.0, 1.0, 0.1));
        }
    }
}
