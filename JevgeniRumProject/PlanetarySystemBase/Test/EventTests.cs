﻿using NUnit.Framework;
using PlanetarySystem.Planetary;

namespace PlanetarySystem.Test
{
    public class EventTests
    {
        private PSController _controller;

        [SetUp]
        public void SetUp()
        {
            _controller = new PSController();
            Logger.Messages.Clear();

            _controller.MakeSolarSystem();
            Assert.AreEqual(false, _controller.GetPlanetarySystem().Autotick);
            _controller.GetPlanetarySystem().Autotick = true;
        }

        [Test]
        public void TestElementApproachPrintReaction()
        {
            const double delta = 1.1;
            _controller.ToggleElementApproachEvent(delta, _controller.GetPlanetarySystem().GetElement(2));
            _controller.ToggleLogger();

            _controller.Tick();

            Assert.AreEqual(true, _controller.GetPlanetarySystem().Autotick);
            Assert.AreEqual("Tekkis huvitav sündmus 1: Simulatsiooni objektid 3. ja 4. " +
                            "on üksteisele lähemal kui delta - " + delta, Logger.GetAllMessages());
        }

        [Test]
        public void TestElementApproachStopReaction()
        {
            const double delta = 0.6;
            _controller.ToggleElementApproachEvent(delta, _controller.GetPlanetarySystem().GetElement(3));
            _controller.TogglePauser();

            _controller.Tick();
            Assert.AreEqual(false, _controller.GetPlanetarySystem().Autotick);
        }

        [Test]
        public void TestElementApproachBothReactions()
        {
            const double delta = 1.1;
            _controller.ToggleElementApproachEvent(delta, _controller.GetPlanetarySystem().GetElement(3));
            _controller.ToggleLogger();
            _controller.TogglePauser();

            _controller.Tick();

            Assert.AreEqual("Tekkis huvitav sündmus 1: Simulatsiooni objektid 4. ja 3. " +
                            "on üksteisele lähemal kui delta - " + delta, Logger.GetAllMessages());
            Assert.AreEqual(false, _controller.GetPlanetarySystem().Autotick);
        }

        [Test]
        public void TestSolarApproachPrintReaction()
        {
            const double delta = 0.4;
            _controller.ToggleSolarApproachEvent(delta);
            _controller.ToggleLogger();

            _controller.Tick();

            Assert.AreEqual("Planeedi rho on 0.39 Delta väärtus " + delta + " Tekkis huvitav sündmus 2:  " +
                            "1. simulatsiooni objektiga (Lähenemine päikesele).", Logger.GetAllMessages(" "));
            Assert.AreEqual(true, _controller.GetPlanetarySystem().Autotick);
        }

        [Test]
        public void TestSolarApproachStopReaction()
        {
            _controller.ToggleSolarApproachEvent(0.4);
            _controller.TogglePauser();

            _controller.Tick();

            Assert.AreEqual(false, _controller.GetPlanetarySystem().Autotick);
        }

        [Test]
        public void TestSolarApproachBothReactions()
        {
            const double delta = 0.4;
            _controller.ToggleSolarApproachEvent(delta);
            _controller.ToggleLogger();
            _controller.TogglePauser();

            _controller.Tick();

            Assert.AreEqual("Planeedi rho on 0.39 Delta väärtus " + delta + " Tekkis huvitav sündmus 2:  " +
                            "1. simulatsiooni objektiga (Lähenemine päikesele).", Logger.GetAllMessages(" "));
            Assert.AreEqual(false, _controller.GetPlanetarySystem().Autotick);
        }

        [Test]
        public void TestBothApproachesPrintReaction()
        {
            const double delta1 = 0.4;
            const double delta2 = 1.1;
            _controller.ToggleSolarApproachEvent(delta1);
            _controller.ToggleElementApproachEvent(delta2, _controller.GetPlanetarySystem().GetElement(3));
            _controller.ToggleLogger();

            _controller.Tick();

            Assert.AreEqual("Planeedi rho on 0.39 Delta väärtus " + delta1 + " Tekkis huvitav sündmus 2:  " +
                            "1. simulatsiooni objektiga (Lähenemine päikesele). Tekkis huvitav sündmus 1:  " +
                            "Simulatsiooni objektid 4. ja 3. on üksteisele lähemal kui delta - " + delta2, Logger.GetAllMessages(" "));
            Assert.AreEqual(true, _controller.GetPlanetarySystem().Autotick);
        }

        [Test]
        public void TestBothApproachesStopReaction()
        {
            const double delta1 = 0.4;
            const double delta2 = 0.6;
            _controller.ToggleSolarApproachEvent(delta1);
            _controller.ToggleElementApproachEvent(delta2, _controller.GetPlanetarySystem().GetElement(3));

            _controller.TogglePauser();
            _controller.Tick();

            Assert.AreEqual(false, _controller.GetPlanetarySystem().Autotick);
        }

        [Test]
        public void TestBothApproachesBothReactions()
        {
            const double delta1 = 0.4;
            const double delta2 = 1.1;
            _controller.ToggleElementApproachEvent(delta2, _controller.GetPlanetarySystem().GetElement(3));
            _controller.ToggleSolarApproachEvent(delta1);

            _controller.ToggleLogger();
            _controller.TogglePauser();

            _controller.Tick();

            Assert.AreEqual("Planeedi rho on 0.39 Delta väärtus " + delta1 + " Tekkis huvitav sündmus 2:  " +
                            "1. simulatsiooni objektiga (Lähenemine päikesele). Tekkis huvitav sündmus 1:  " +
                            "Simulatsiooni objektid 4. ja 3. on üksteisele lähemal kui delta - " + delta2, Logger.GetAllMessages(" "));
            Assert.AreEqual(false, _controller.GetPlanetarySystem().Autotick);
        }
    }
}