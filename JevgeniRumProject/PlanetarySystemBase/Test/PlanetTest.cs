﻿using System;
using NUnit.Framework;
using PlanetarySystem.Planetary;

namespace PlanetarySystem.Test
{
    [TestFixture]
    internal class PlanetTest
    {
        [Test]
        public void TestSimple()
        {
            Planet p = new Planet(1.0, 1.0, 0.1);
            Planet p2 = new Planet(4.0, 5.0, 0.2);
            Assert.That(p.Distance(p2), Is.EqualTo(5.0).Within(0.0001));
            p.Tick();
            Assert.That(p.GetTheta(), Is.EqualTo(Math.PI/4 + 0.1).Within(0.0001));
        }
    }
}