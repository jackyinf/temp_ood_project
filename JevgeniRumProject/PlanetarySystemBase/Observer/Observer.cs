﻿using PlanetarySystem.Planetary;

namespace PlanetarySystem.Observer
{
    public abstract class Observer
    {
        public abstract void Update(Event eEvent);
    }
}
