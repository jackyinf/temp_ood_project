﻿using PlanetarySystem.Planetary;

namespace PlanetarySystem.Observer
{
    public class LoggerObserver : Observer
    {
        public override void Update(Event eEvent)
        {
            eEvent.Print();
        }
    }
}
