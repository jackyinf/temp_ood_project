﻿using PlanetarySystem.Planetary;

namespace PlanetarySystem.Observer
{
    public class PauserObserver : Observer
    {
        public override void Update(Event eEvent)
        {
            eEvent.PlanetarySystem.Autotick = false;
        }
    }
}
