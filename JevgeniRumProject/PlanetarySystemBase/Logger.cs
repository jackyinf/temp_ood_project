﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PlanetarySystem
{
    public static class Logger
    {
        public static List<string> Messages = new List<string>();

        public static void WriteLine(string message)
        {
            Messages.Add(message);
            Debug.WriteLine(message);
        }

        public static string GetAllMessages(string separator = "")
        {
            return string.Join(separator, Messages);
        }
    }
}
