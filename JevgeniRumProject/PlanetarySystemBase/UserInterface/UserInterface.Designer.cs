﻿using System.ComponentModel;
using System.Windows.Forms;

namespace PlanetarySystem.UserInterface
{
    partial class UserInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonTick = new System.Windows.Forms.Button();
            this.buttonStartAutotick = new System.Windows.Forms.Button();
            this.buttonLaunchSpaceship = new System.Windows.Forms.Button();
            this.checkBoxElementApproach = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSolarApproachDelta = new System.Windows.Forms.TextBox();
            this.checkBoxSolarApproach = new System.Windows.Forms.CheckBox();
            this.labelDelta = new System.Windows.Forms.Label();
            this.labelPlanetID = new System.Windows.Forms.Label();
            this.textBoxElementApproachPlanetID = new System.Windows.Forms.TextBox();
            this.textBoxElementApproachDelta = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxPauser = new System.Windows.Forms.CheckBox();
            this.checkBoxLogger = new System.Windows.Forms.CheckBox();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonTick
            // 
            this.buttonTick.Location = new System.Drawing.Point(23, 3);
            this.buttonTick.Name = "buttonTick";
            this.buttonTick.Size = new System.Drawing.Size(107, 23);
            this.buttonTick.TabIndex = 0;
            this.buttonTick.Text = "Tick";
            this.buttonTick.UseVisualStyleBackColor = true;
            this.buttonTick.Click += new System.EventHandler(this.buttonTick_Click);
            // 
            // buttonStartAutotick
            // 
            this.buttonStartAutotick.Location = new System.Drawing.Point(136, 3);
            this.buttonStartAutotick.Name = "buttonStartAutotick";
            this.buttonStartAutotick.Size = new System.Drawing.Size(107, 23);
            this.buttonStartAutotick.TabIndex = 1;
            this.buttonStartAutotick.Text = "Toggle autotick";
            this.buttonStartAutotick.UseVisualStyleBackColor = true;
            this.buttonStartAutotick.Click += new System.EventHandler(this.buttonAutotick_Click);
            // 
            // buttonLaunchSpaceship
            // 
            this.buttonLaunchSpaceship.Location = new System.Drawing.Point(249, 3);
            this.buttonLaunchSpaceship.Name = "buttonLaunchSpaceship";
            this.buttonLaunchSpaceship.Size = new System.Drawing.Size(107, 23);
            this.buttonLaunchSpaceship.TabIndex = 2;
            this.buttonLaunchSpaceship.Text = "Launch spaceship";
            this.buttonLaunchSpaceship.UseVisualStyleBackColor = true;
            this.buttonLaunchSpaceship.Click += new System.EventHandler(this.buttonLaunchSpaceship_Click);
            // 
            // checkBoxElementApproach
            // 
            this.checkBoxElementApproach.AutoSize = true;
            this.checkBoxElementApproach.Enabled = false;
            this.checkBoxElementApproach.Location = new System.Drawing.Point(16, 19);
            this.checkBoxElementApproach.Name = "checkBoxElementApproach";
            this.checkBoxElementApproach.Size = new System.Drawing.Size(112, 17);
            this.checkBoxElementApproach.TabIndex = 3;
            this.checkBoxElementApproach.Text = "Element approach";
            this.checkBoxElementApproach.UseVisualStyleBackColor = true;
            this.checkBoxElementApproach.CheckedChanged += new System.EventHandler(this.checkBoxElementApproach_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxSolarApproachDelta);
            this.groupBox1.Controls.Add(this.checkBoxSolarApproach);
            this.groupBox1.Controls.Add(this.labelDelta);
            this.groupBox1.Controls.Add(this.labelPlanetID);
            this.groupBox1.Controls.Add(this.textBoxElementApproachPlanetID);
            this.groupBox1.Controls.Add(this.textBoxElementApproachDelta);
            this.groupBox1.Controls.Add(this.checkBoxElementApproach);
            this.groupBox1.Location = new System.Drawing.Point(8, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(443, 74);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Events";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(135, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Delta";
            // 
            // textBoxSolarApproachDelta
            // 
            this.textBoxSolarApproachDelta.Location = new System.Drawing.Point(176, 49);
            this.textBoxSolarApproachDelta.Name = "textBoxSolarApproachDelta";
            this.textBoxSolarApproachDelta.Size = new System.Drawing.Size(100, 20);
            this.textBoxSolarApproachDelta.TabIndex = 9;
            this.textBoxSolarApproachDelta.TextChanged += new System.EventHandler(this.textBoxSolarApproachDelta_TextChanged);
            // 
            // checkBoxSolarApproach
            // 
            this.checkBoxSolarApproach.AutoSize = true;
            this.checkBoxSolarApproach.Enabled = false;
            this.checkBoxSolarApproach.Location = new System.Drawing.Point(16, 51);
            this.checkBoxSolarApproach.Name = "checkBoxSolarApproach";
            this.checkBoxSolarApproach.Size = new System.Drawing.Size(98, 17);
            this.checkBoxSolarApproach.TabIndex = 8;
            this.checkBoxSolarApproach.Text = "Solar approach";
            this.checkBoxSolarApproach.UseVisualStyleBackColor = true;
            this.checkBoxSolarApproach.CheckedChanged += new System.EventHandler(this.checkBoxSolarApproach_CheckedChanged);
            // 
            // labelDelta
            // 
            this.labelDelta.AutoSize = true;
            this.labelDelta.Location = new System.Drawing.Point(135, 20);
            this.labelDelta.Name = "labelDelta";
            this.labelDelta.Size = new System.Drawing.Size(32, 13);
            this.labelDelta.TabIndex = 7;
            this.labelDelta.Text = "Delta";
            // 
            // labelPlanetID
            // 
            this.labelPlanetID.AutoSize = true;
            this.labelPlanetID.Location = new System.Drawing.Point(282, 20);
            this.labelPlanetID.Name = "labelPlanetID";
            this.labelPlanetID.Size = new System.Drawing.Size(51, 13);
            this.labelPlanetID.TabIndex = 6;
            this.labelPlanetID.Text = "Planet ID";
            // 
            // textBoxElementApproachPlanetID
            // 
            this.textBoxElementApproachPlanetID.Location = new System.Drawing.Point(337, 17);
            this.textBoxElementApproachPlanetID.Name = "textBoxElementApproachPlanetID";
            this.textBoxElementApproachPlanetID.Size = new System.Drawing.Size(100, 20);
            this.textBoxElementApproachPlanetID.TabIndex = 5;
            this.textBoxElementApproachPlanetID.TextChanged += new System.EventHandler(this.textBoxElementApproachPlanetID_TextChanged);
            // 
            // textBoxElementApproachDelta
            // 
            this.textBoxElementApproachDelta.Location = new System.Drawing.Point(176, 17);
            this.textBoxElementApproachDelta.Name = "textBoxElementApproachDelta";
            this.textBoxElementApproachDelta.Size = new System.Drawing.Size(100, 20);
            this.textBoxElementApproachDelta.TabIndex = 4;
            this.textBoxElementApproachDelta.TextChanged += new System.EventHandler(this.textBoxElementApproachDelta_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.checkBoxPauser);
            this.groupBox2.Controls.Add(this.checkBoxLogger);
            this.groupBox2.Location = new System.Drawing.Point(457, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 74);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Actions";
            // 
            // checkBoxPauser
            // 
            this.checkBoxPauser.AutoSize = true;
            this.checkBoxPauser.Location = new System.Drawing.Point(6, 51);
            this.checkBoxPauser.Name = "checkBoxPauser";
            this.checkBoxPauser.Size = new System.Drawing.Size(105, 17);
            this.checkBoxPauser.TabIndex = 1;
            this.checkBoxPauser.Text = "Pause simulation";
            this.checkBoxPauser.UseVisualStyleBackColor = true;
            this.checkBoxPauser.CheckedChanged += new System.EventHandler(this.checkBoxPauser_CheckedChanged);
            // 
            // checkBoxLogger
            // 
            this.checkBoxLogger.AutoSize = true;
            this.checkBoxLogger.Location = new System.Drawing.Point(6, 19);
            this.checkBoxLogger.Name = "checkBoxLogger";
            this.checkBoxLogger.Size = new System.Drawing.Size(99, 17);
            this.checkBoxLogger.TabIndex = 0;
            this.checkBoxLogger.Text = "Print to console";
            this.checkBoxLogger.UseVisualStyleBackColor = true;
            this.checkBoxLogger.CheckedChanged += new System.EventHandler(this.checkBoxLogger_CheckedChanged);
            // 
            // panelBottom
            // 
            this.panelBottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBottom.Controls.Add(this.groupBox2);
            this.panelBottom.Controls.Add(this.groupBox1);
            this.panelBottom.Location = new System.Drawing.Point(12, 321);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(719, 100);
            this.panelBottom.TabIndex = 2;
            // 
            // UserInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 433);
            this.Controls.Add(this.buttonLaunchSpaceship);
            this.Controls.Add(this.buttonStartAutotick);
            this.Controls.Add(this.buttonTick);
            this.Controls.Add(this.panelBottom);
            this.Name = "UserInterface";
            this.Text = "UserInterface";
            this.Load += new System.EventHandler(this.UserInterface_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Button buttonTick;
        private Button buttonStartAutotick;
        private Button buttonLaunchSpaceship;
        private CheckBox checkBoxElementApproach;
        private GroupBox groupBox1;
        private Label label1;
        private TextBox textBoxSolarApproachDelta;
        private CheckBox checkBoxSolarApproach;
        private Label labelDelta;
        private Label labelPlanetID;
        private TextBox textBoxElementApproachPlanetID;
        private TextBox textBoxElementApproachDelta;
        private GroupBox groupBox2;
        private CheckBox checkBoxPauser;
        private CheckBox checkBoxLogger;
        private Panel panelBottom;
    }
}