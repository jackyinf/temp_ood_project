﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using PlanetarySystem.Planetary;

namespace PlanetarySystem.UserInterface
{
    public partial class UserInterface : Form
    {
        private const int SLEEP_BETWEEN_TICKS = 50;
        private static bool _tick = true;
        private readonly PSController _controller = new PSController();

        private double _cx;
        private double _cy;
        private double _planetWidth;
        private double _zoom;

        public UserInterface()
        {
            InitializeComponent();
        }

        private void PaintPlanets(Graphics g)
        {
            DrawPlanets(g);
        }

        private void UserInterface_Load(object sender, EventArgs e)
        {
            _controller.MakeSolarSystem();
            _planetWidth = 3;
            _cx = 350.0;
            _cy = 350.0;
            _zoom = 7.0;
            BackColor = Color.Black;
            panelBottom.BackColor = Color.WhiteSmoke;
            Width = 800;
            Height = 800;

            var newThread = new Thread(Run);
            newThread.Start();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = CreateGraphics();
            PaintPlanets(g);
        }

        private void Run()
        {
            while (true)
            {
                try
                {
                    if (_tick)
                    {
                        BeginInvoke((MethodInvoker) delegate
                        {
                            _controller.Tick();
                            Refresh();
                        });
                        if (!_controller.GetPlanetarySystem().Autotick)
                        {
                            _tick = false;
                        }
                    }


                    Thread.Sleep(SLEEP_BETWEEN_TICKS);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private void DrawPlanets(Graphics g)
        {
            // Draw sun
            double[] sunCoordinates = ConvCoords(0, 0);
            g.DrawEllipse(new Pen(Color.Orange), (int)sunCoordinates[0], (int)sunCoordinates[1], 5, 5);

            // Draw elements
            for (int i = 0; i < _controller.GetPlanetarySystem().Size(); i++)
            {
                DrawElement(g, i);
            }
        }

        private void DrawElement(Graphics g, int elementId)
        {
            var element = _controller.GetPlanetarySystem().GetElement(elementId);

            double[] newCoordinates = ConvCoords(element.GetX(), element.GetY());
            g.DrawEllipse(element is Planet ?  new Pen(Color.Red) : new Pen(Color.DeepSkyBlue), (int)newCoordinates[0], (int)newCoordinates[1], (int)_planetWidth, (int)_planetWidth);
        }

        private double[] ConvCoords(double x, double y)
        {
            double x0 = (_cx + x*_zoom);
            double y0 = (_cy + y*_zoom);
            double x1 = x0 + _planetWidth;
            double y1 = y0 + _planetWidth;
            return new[]
            {
                x0, y0, x1, y1
            };
        }

        private void buttonTick_Click(object sender, EventArgs e)
        {
            _controller.GetPlanetarySystem().Autotick = false;
            _tick = true;
        }

        private void buttonAutotick_Click(object sender, EventArgs e)
        {
            if (_controller.GetPlanetarySystem().Autotick)
            {
                _controller.GetPlanetarySystem().Autotick = false;
                _tick = true;
            }
            else
            {
                _controller.GetPlanetarySystem().Autotick = true;
                _tick = true;
            }
        }

        private void buttonLaunchSpaceship_Click(object sender, EventArgs e)
        {
            _controller.Launch(_controller.GetPlanetarySystem().GetElement(0), 0.5, 0.5);
        }

        private void checkBoxElementApproach_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBoxElementApproach.Checked)
            {
                _controller.ToggleElementApproachEvent(0.0, null);
                return;
            }

            try
            {
                var delta = double.Parse(textBoxElementApproachDelta.Text);
                var planetId = int.Parse(textBoxElementApproachPlanetID.Text);
                var planet = _controller.GetPlanetarySystem().GetElement(planetId);
                _controller.ToggleElementApproachEvent(delta, planet);
            }
            catch (Exception exception)
            {
                Debug.Write("Valed sisestusandmed. ");
                Debug.WriteLine(exception.Message);
            }
        }

        private void checkBoxSolarApproach_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBoxSolarApproach.Checked)
            {
                _controller.ToggleSolarApproachEvent(null);
                return;
            }

            try
            {
                var delta = double.Parse(textBoxSolarApproachDelta.Text);
                _controller.ToggleSolarApproachEvent(delta);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Valed sisestusandmed. ");
                Debug.WriteLine(exception.Message);
            }
        }

        private void checkBoxLogger_CheckedChanged(object sender, EventArgs e)
        {
            _controller.ToggleLogger();
        }

        private void checkBoxPauser_CheckedChanged(object sender, EventArgs e)
        {
            _controller.TogglePauser();
        }


        private void textBoxElementApproachDelta_TextChanged(object sender, EventArgs e)
        {
            checkBoxElementApproach.Enabled = TryParseDelta(textBoxElementApproachDelta.Text) && TryParsePlanetId(textBoxElementApproachPlanetID.Text);
        }

        private void textBoxElementApproachPlanetID_TextChanged(object sender, EventArgs e)
        {
            checkBoxElementApproach.Enabled = TryParseDelta(textBoxElementApproachDelta.Text) && TryParsePlanetId(textBoxElementApproachPlanetID.Text);
        }

        private void textBoxSolarApproachDelta_TextChanged(object sender, EventArgs e)
        {
            checkBoxSolarApproach.Enabled = TryParseDelta(textBoxSolarApproachDelta.Text);
        }

        private bool TryParseDelta(string deltaStr)
        {
            try
            {
                double.Parse(deltaStr);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool TryParsePlanetId(string planetIdStr)
        {
            try
            {
                _controller.GetPlanetarySystem().GetElement(int.Parse(planetIdStr));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}