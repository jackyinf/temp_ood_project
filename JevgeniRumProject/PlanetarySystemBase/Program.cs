﻿using System.Windows.Forms;

namespace PlanetarySystem {
    internal class Program
    {
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new UserInterface.UserInterface());
        }
    }
}
