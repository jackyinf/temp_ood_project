﻿using System.Diagnostics;

namespace PlanetarySystem.Planetary
{
    public class ElementApproach : Event
    {
        public PointSimulationElement LastPointSimulationElement { get; private set; }

        public ElementApproach(PointSimulationElement pointSimulationElement, double delta, PlanetarySystem<PointSimulationElement> planetarySystem)
            : base(pointSimulationElement, delta, planetarySystem)
        {
        }

        public override bool IsInterestingEvent(PointSimulationElement element)
        {
            LastPointSimulationElement = element;
            return LastPointSimulationElement != Planet && LastPointSimulationElement.Distance(Planet) < Delta;
        }

        public override void Print()
        {
            Logger.WriteLine("Tekkis huvitav sündmus 1: ");
            Logger.WriteLine("Simulatsiooni objektid " + (PlanetarySystem.Elements.IndexOf(Planet) + 1) +
                            ". ja " + (PlanetarySystem.Elements.IndexOf(LastPointSimulationElement) + 1) +
                            ". on üksteisele lähemal kui delta - " + Delta);
        }
    }
}
