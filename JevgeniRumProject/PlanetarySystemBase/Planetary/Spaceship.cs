﻿namespace PlanetarySystem.Planetary
{
    public class Spaceship : PointSimulationElement
    {
        private readonly double _dx;
        private readonly double _dy;

        public Spaceship(double x, double y, double dx, double dy) : base(x, y)
        {
            _dx = dx;
            _dy = dy;
        }

        public override void Tick()
        {
            Translate(_dx, _dy);
        }
    }
}
