﻿using System.Collections.Generic;

namespace PlanetarySystem.Planetary
{
    public class EventManager<T> where T : Event
    {
        public List<Event> Events { get; private set; }
        public List<Observer.Observer> Observers { get; private set; }

        public EventManager()
        {
            Events = new List<Event>();
            Observers = new List<Observer.Observer>();
        }

        public void CheckEvents(PointSimulationElement pointSimulationElement)
        {
            foreach (var @event in Events)
            {
                if (@event.IsInterestingEvent(pointSimulationElement))
                    NotifyObservers(@event);
            }
        }

        private void NotifyObservers(Event eEvent)
        {
            foreach (var observer in Observers)
            {
                observer.Update(eEvent);
            }
        }
    }
}
