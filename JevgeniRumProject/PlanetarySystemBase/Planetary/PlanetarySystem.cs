﻿using System.Collections.Generic;

namespace PlanetarySystem.Planetary
{
    public class PlanetarySystem<T> : ISimulationElement where T : ISimulationElement
    {
        public readonly List<T> Elements;
        public bool Autotick { get; set; }
        public EventManager<Event> EventManager { get; private set; }

        public PlanetarySystem()
        {
            Elements = new List<T>();
            Autotick = false;
            EventManager = new EventManager<Event>();
        }

        public void Tick()
        {
            foreach (var element in Elements)
            {
                element.Tick();
                EventManager.CheckEvents(element as PointSimulationElement);
            }
        }

        public void Append(T element)
        {
            Elements.Add(element);
        }

        public T GetElement(int indeks)
        {
            return Elements[indeks];
        }

        public int Size()
        {
            return Elements.Count;
        }
    }
}