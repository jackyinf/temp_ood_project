﻿namespace PlanetarySystem.Planetary
{
    public abstract class Event
    {
        protected double Delta { get; private set; }
        public PlanetarySystem<PointSimulationElement> PlanetarySystem { get; private set; }
        protected PointSimulationElement Planet { get; private set; }
        
        protected Event( PointSimulationElement planet, double delta, PlanetarySystem<PointSimulationElement> planetarySystem)
        {
            Planet = planet;
            Delta = delta;
            PlanetarySystem = planetarySystem;
        }

        protected Event(double delta, PlanetarySystem<PointSimulationElement> planetarySystem)
        {
            Delta = delta;
            PlanetarySystem = planetarySystem;
        }

        public abstract bool IsInterestingEvent(PointSimulationElement element);
        public abstract void Print();
    }
}
