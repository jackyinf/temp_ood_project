﻿using System;
using System.Runtime.CompilerServices;
using PlanetarySystem.Observer;

namespace PlanetarySystem.Planetary
{
    public class PSController
    {
        // T määrab, mitu atomaarset Tick sammu on Maa aastas
        private const double T = 4;

        // Planeedisüsteem, millel simulatsioon toimub
        private PlanetarySystem<PointSimulationElement> _planetarySystem;


        // Fassaadmeetod, mis loob uue planeedisüsteemi
        public void MakeSolarSystem()
        {
            if (GetPlanetarySystem() == null)
            {
                _planetarySystem = new PlanetarySystem<PointSimulationElement>();
                GetPlanetarySystem().Append(new Planet(0.39, 0.0, (2.0*Math.PI)/(87.97/365.26)*T));
                GetPlanetarySystem().Append(new Planet(0.72, 0.0, (2.0*Math.PI)/(227.7/365.26)*T));
                GetPlanetarySystem().Append(new Planet(1.0, 0.0, (2.0*Math.PI)/(1.0)*T));
                GetPlanetarySystem().Append(new Planet(1.52, 0.0, (2.0*Math.PI)/(686.98/365.26)*T));
                GetPlanetarySystem().Append(new Planet(5.2, 0.0, (2.0*Math.PI)/(11.86)*T));
                GetPlanetarySystem().Append(new Planet(9.54, 0.0, (2.0*Math.PI)/(29.46)*T));
                GetPlanetarySystem().Append(new Planet(19.18, 0.0, (2.0*Math.PI)/(84.01)*T));
                GetPlanetarySystem().Append(new Planet(30.06, 0.0, (2.0*Math.PI)/(164.81)*T));
                GetPlanetarySystem().Append(new Planet(39.75, 0.0, (2.0*Math.PI)/(247.7)*T));
            }
        }


        // Fassaadmeetod, mis liigutab planeedisüsteemi objekte ühe atomaarse sammu võrra
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Tick()
        {
            GetPlanetarySystem().Tick();
        }

        public PlanetarySystem<PointSimulationElement> GetPlanetarySystem()
        {
            return _planetarySystem;
        }

        public void ToggleSolarApproachEvent(double? delta)
        {
            foreach (var @event in GetPlanetarySystem().EventManager.Events)
            {
                if (@event is SolarApproach)
                {
                    GetPlanetarySystem().EventManager.Events.Remove(@event);
                    return;
                }
            }
            if (delta != null)
                GetPlanetarySystem().EventManager.Events.Add(new SolarApproach(delta.Value, GetPlanetarySystem()));
        }

        public void ToggleElementApproachEvent(double delta, PointSimulationElement element)
        {
            foreach (var @event in GetPlanetarySystem().EventManager.Events)
            {
                if (@event is ElementApproach)
                {
                    GetPlanetarySystem().EventManager.Events.Remove(@event);
                    return;
                }
            }
            if (!Double.IsNaN(delta))
                GetPlanetarySystem().EventManager.Events.Add(new ElementApproach(element, delta, GetPlanetarySystem()));
        }

        public void Launch(PointSimulationElement element, double dx, double dy)
        {
            var spaceship = new Spaceship(element.GetX(), element.GetY(), dx, dy);
            GetPlanetarySystem().Append(spaceship);
        }

        public void TogglePauser()
        {
            foreach (var observer in GetPlanetarySystem().EventManager.Observers)
            {
                if (observer is PauserObserver)
                {
                    GetPlanetarySystem().EventManager.Observers.Remove(observer);
                    return;
                }
            }
            GetPlanetarySystem().EventManager.Observers.Add(new PauserObserver());
        }

        public void ToggleLogger()
        {
            foreach (var observer in GetPlanetarySystem().EventManager.Observers)
            {
                if (observer is LoggerObserver)
                {
                    GetPlanetarySystem().EventManager.Observers.Remove(observer);
                    return;
                }
            }
            GetPlanetarySystem().EventManager.Observers.Add(new LoggerObserver());
        }
    }
}