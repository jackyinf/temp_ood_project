﻿using System.Diagnostics;

namespace PlanetarySystem.Planetary
{
    public class SolarApproach : Event
    {
        public PointSimulationElement LastPointSimulationElement { get; private set; }

        public SolarApproach(double delta, PlanetarySystem<PointSimulationElement> planetarySystem) : base(delta, planetarySystem)
        {
        }

        public override bool IsInterestingEvent(PointSimulationElement element)
        {
            LastPointSimulationElement = element;
            return element.GetRho() < Delta;
        }

        public override void Print()
        {
            Logger.WriteLine("Planeedi rho on " + LastPointSimulationElement.GetRho());
            Logger.WriteLine("Delta väärtus " + Delta);
            Logger.WriteLine("Tekkis huvitav sündmus 2: ");
            Logger.WriteLine((PlanetarySystem.Elements.IndexOf(LastPointSimulationElement) + 1) + ". simulatsiooni objektiga (Lähenemine päikesele).");
        }
    }
}
